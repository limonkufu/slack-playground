# Slack API Playground

This repository includes personal experiments/tests that are done with the Slack API.


## Setup

Create a [ngrok](https://ngrok.com/) server and set up following in the Slack Workspace:

**For Slash command and Message Shortcut bot:**

- Create an App
- Give Permissions
  - bot commands
  - bot chat:write
- Add shortcuts from src/app.py file

**For PythonOnboarding bot to respond to Slack Events API:**
  
- Create an App
- Give Permissions
  - bot commands

Follow the [official tutorial](https://github.com/slackapi/python-slackclient/tree/main/tutorial)
