import hashlib
import hmac
import logging
import os
import time
from crypt import methods
from pkgutil import get_data

from flask import Flask, abort, jsonify, request
from flask.helpers import make_response
from requests import status_codes
from slack import WebClient
from slack.errors import SlackApiError
from slack.signature import SignatureVerifier
from time import sleep
import json

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)


@app.route("/create", methods=["POST"])
def create_command():
    print("Create command received")

    timestamp = request.headers["X-Slack-Request-Timestamp"]
    request_body = request.get_data().decode()
    slack_signature = request.headers["X-Slack-Signature"]
    if (int(time.time()) - int(timestamp)) > 60:
        print("Verification failed. Request is out of date.")
        abort(400)

    basestring = f"v0:{timestamp}:{request_body}".encode("utf-8")
    my_signature = (
        "v0=" + hmac.new(SLACK_SIGNATURE, basestring, hashlib.sha256).hexdigest()
    )

    if not hmac.compare_digest(my_signature, slack_signature):
        print("Verification failed. Signature invalid!")
        abort(400)

    return jsonify(response_type="in_channel", text="Create command invoked!",)


@app.route("/create_ep", methods=["POST"])
def create_ep_command():
    print("Create EP command received")

    timestamp = request.headers["X-Slack-Request-Timestamp"]
    request_body = request.get_data().decode()
    slack_signature = request.headers["X-Slack-Signature"]
    if (int(time.time()) - int(timestamp)) > 60:
        print("Verification failed. Request is out of date.")
        abort(400)

    basestring = f"v0:{timestamp}:{request_body}".encode("utf-8")
    my_signature = (
        "v0=" + hmac.new(SLACK_SIGNATURE, basestring, hashlib.sha256).hexdigest()
    )

    if not hmac.compare_digest(my_signature, slack_signature):
        print("Verification failed. Signature invalid!")
        abort(400)

    return jsonify(response_type="ephemeral", text="Create EP command invoked!",)


@app.route("/create_mes", methods=["POST"])
def create_mes_command():
    print("Create MES command received")

    timestamp = request.headers["X-Slack-Request-Timestamp"]
    request_body = request.get_data().decode()
    slack_signature = request.headers["X-Slack-Signature"]
    if (int(time.time()) - int(timestamp)) > 60:
        print("Verification failed. Request is out of date.")
        abort(400)

    basestring = f"v0:{timestamp}:{request_body}".encode("utf-8")
    my_signature = (
        "v0=" + hmac.new(SLACK_SIGNATURE, basestring, hashlib.sha256).hexdigest()
    )

    if not hmac.compare_digest(my_signature, slack_signature):
        print("Verification failed. Signature invalid!")
        abort(400)

    try:
        response = slack_client.chat_postMessage(
            channel=request.form["channel_id"], text="Create MES command invoked!",
        )
    except SlackApiError as e:
        assert e.response["error"]
        return make_response("", e.response.status_code)

    return make_response("", response.status_code)


@app.route("/shortcuts", methods=["POST"])
def slack_app():
    if not signature_verifier.is_valid_request(request.get_data(), request.headers):
        return make_response("invalid request", 403)

    if "payload" in request.form:
        payload = json.loads(request.form["payload"])

        if (
            payload["type"] == "message_action"
            and payload["callback_id"] == "create_ticket"
        ):
            message_rcvd = payload["message"]
            try:
                api_response = slack_client.views_open(
                    trigger_id=payload["trigger_id"],
                    view={
                        "type": "modal",
                        "callback_id": "modal-id",
                        "title": {"type": "plain_text", "text": "Awesome Modal"},
                        "submit": {"type": "plain_text", "text": "Submit"},
                        "close": {"type": "plain_text", "text": "Cancel"},
                        "blocks": [
                            {
                                "type": "input",
                                "block_id": "b-id",
                                "label": {"type": "plain_text", "text": "Is there anything you want to change?",},
                                "element": {
                                    "action_id": "a-id",
                                    "type": "plain_text_input",
                                    "initial_value": message_rcvd['text']
                                },
                            }
                        ],
                    },
                )
                return make_response("", 200)
            except SlackApiError as e:
                code = e.response["error"]
                return make_response(f"Failed to open a modal due to {code}", 200)

        if (
            payload["type"] == "view_submission"
            and payload["view"]["callback_id"] == "modal-id"
        ):
            # Handle a data submission request from the modal
            submitted_data = payload["view"]["state"]["values"]
            print(
                submitted_data
            )  # {'b-id': {'a-id': {'type': 'plain_text_input', 'value': 'your input'}}}
            return make_response("", 200)

    return make_response("", 404)


if __name__ == "__main__":
    SLACK_BOT_TOKEN = os.environ["SLACK_BOT_TOKEN"]
    SLACK_SIGNATURE = os.environ["SLACK_SIGNING_SECRET"]
    signature_verifier = SignatureVerifier(SLACK_SIGNATURE)
    #   SLACK_SIGNATURE = os.environ['SLACK_SIGNATURE']
    slack_client = WebClient(SLACK_BOT_TOKEN)
    #   verifier = SignatureVerifier(SLACK_SIGNATURE)

    app.run(port=3000)

